#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/*
 * Hier werden zunächst unsere Konstanten als Makros definiert.
 */
#define pi 3.14159265358979323846
#define m_e 9.10938356*pow(10,-31) //EINHEIT: [kg]
#define h_quer 1.054571800*pow(10,-34) //EINHEIT: [Js]
#define eV  1.6021766208*pow(10,-19) //EINHEIT: [J]
#define genauigkeit 0.000000001 //Unsere Genauigkeit, die wir durch Testen ermittelt haben, um ein gutes Gleichgewicht zwischen Rechenzeit und Funktionalität zu erhalten

/*
 * Deklaration aller Funktionen, aus Übersichts- und Funktionsgründen
 */
void eigenwerteBerechnen(int, char**);
void eingabe(double*, double*, int, char**);
double bisection(double, double, double, short);
double f(double, double, short);
void bubblesort(double*, char*, int);
void ausgabe(double, double, double*, char*, int, int);

int main(int argc, char *argv[])
{ 
	/*
	 * PROGRAMM WIRD AUFGERUFEN MIT ./main.c [Breite a in pm] [Tiefe V0 in eV]
	 * JEDOCH ist dies nicht notwendig, wenn zu wenig Parameter übergeben werden bzw
	 * gar keine, dann wird einfach im Program nach den Parametern gefragt.
	 * */
    eigenwerteBerechnen(argc, argv); //unsere Hauptfunktion, von hier aus rufen wir alle anderen Funktionen auf
    return 0;
}

/*
 * Diese Funktion berechnet die Eigenwerte und startet die Ausgabe
 * Parameter: Anzahl der vom Nutzer übergebene Argumente, Zeiger zu den Argumenten
 * Return: kein Return
 */
void eigenwerteBerechnen(int argc, char **argv){
	double a; //hier speichern wir unsere Potentialtopfbreite a in [m] (nach der Umrechnung)
	double v_0; //hier speichern wir unsere Potentialtopftiefe V0 in [J] (nach der Umrechnung)
	
	eingabe(&a, &v_0, argc, argv);	
	
	double xi=(sqrt(2*m_e*v_0)*a)/(h_quer); //Xi berechnen nach der gegebenen Formel
	int anzahlGerade = (int)(xi/pi+1); //Wieviele gerade Nullstellen haben wir?
	int anzahlUngerade = (int)(xi/pi+0.5); //Wieviele ungerade Nullstellen haben wir?    
	
	double nullstellen[anzahlGerade+anzahlUngerade]; //hier werden alle Nullstellen gespeichert, an den ersten Stellen die geraden, danach die ungeraden
	char symmetrie[anzahlGerade+anzahlUngerade]; //hier wird gespeichert, ob meine i-te Nullstelle gerade oder ungerade ist, wichtig für die Ausgabe später
	
	for(int i = 0; i<anzahlGerade; i++){
		if((i+0.5)*pi-genauigkeit <= xi){
			nullstellen[i] = bisection(i*pi+genauigkeit, (i+0.5)*pi-genauigkeit,xi,0); //wir setzen unsere Intervalle, wie in der Theorie und führen die Bisection aus
			symmetrie[i] = 'g'; //in dieser Schleife wissen wir, dass alle Nullstellen gerade sind
		}else{ //Falls wir über das Ziel hinausschießen, wird als rechte Grenze xi übergeben
			nullstellen[i] = bisection(i*pi+genauigkeit, xi,xi,0);
			symmetrie[i] = 'g';
		}
	}
	
	for(int i = 0; i<anzahlUngerade; i++){
		if((i+1)*pi-genauigkeit <= xi){
			nullstellen[i+anzahlGerade] = bisection((i+0.5)*pi+genauigkeit, (i+1)*pi-genauigkeit,xi,1);
			symmetrie[i+anzahlGerade] = 'u';
		}else{
			nullstellen[i+anzahlGerade] = bisection(i*pi+genauigkeit, xi,xi,1);
			symmetrie[i+anzahlGerade] = 'u';
		}
		
	}
	for(int i=0; i<anzahlGerade+anzahlUngerade; i++){
		nullstellen[i] = ((pow(nullstellen[i],2)*pow(h_quer,2))/(2*m_e*pow(a,2)))-v_0; //aus den gefundenen z0 berechnen wir die Energieeigenwerte
		nullstellen[i] /= eV; //und rechnen diese in eV um, für die korrekte Ausgabe später
	}
	bubblesort(nullstellen, symmetrie, anzahlGerade+anzahlUngerade); //da erst die geraden und dann die ungeraden Nullstellen kommen, werden sie nun nach aufsteigendem Wert sortiert
	
	printf("Berechnung erfolgreich beendet.\n");
	ausgabe(a, v_0, nullstellen, symmetrie, anzahlGerade, anzahlUngerade);
}

/*
 * Diese Funktion regelt die Eingabe und rechnet die eingegebenen Werte sofort in die richtige Einheit um
 * Parameter: Zeiger auf unsere Potentialtopfbreite, Zeiger auf unsere Potentialtopftiefe, Anzahl der vom Nutzer übergebene Argumente, Zeiger zu den Argumenten
 * Return: kein Return
 */
void eingabe(double *a, double *v_0, int argc, char **argv){
	
	if(argc < 3){ //wenn der Nutzer weniger als 2 Argumente zur Ausführungen verwendet hat, brauchen wir mehr Information
		printf("Gebe Breitenparameter a in [pm] ein:\n");
		scanf("%lf",a);   
		printf("Gebe Potential Vo in [eV] ein:\n");
		scanf("%lf",v_0);
	}
	else{
		sscanf(argv[1], "%lf", a);
		sscanf(argv[2], "%lf", v_0);
	}
	printf("\nBerechnung initiiert.\n");
    *a*=pow(10,-12); //UMRECHNUNG von a in [m]
    *v_0*=eV; //UMRECHNUNG von v_0 in [J]
}

/*
 * Diese Funktion führt die Bisection für den geraden, sowie ungeraden Fall durch
 * Parameter: linke Intervallgrenze, rechte Intervallgrenze, berechnetes xi, 0 für gerade oder 1 für ungerade Symmetrie
 * Return: unsere z-Nullstelle als double
 */
double bisection(double x_1, double x_2, double xi, short symmetrie){
	double x_mitte;
	while(f(x_1, xi, symmetrie)*f(x_2, xi, symmetrie) < 0 && x_2-x_1 > genauigkeit){ //Ändert sich im Intervall das Vorzeichen und ist unser Intervall noch groß genug?
		x_mitte = (x_1+x_2)*0.5;
		if(f(x_1, xi, symmetrie)*f(x_mitte, xi, symmetrie) < 0){ //Ändert sich das Vorzeichen zwischen der linken Grenze und der Mitte?
			x_2 = x_mitte; //dann ist die Mitte nun unsere neue rechte Grenze
		}else if(f(x_2, xi, symmetrie)*f(x_mitte, xi, symmetrie) < 0){ //umgekehrte Abfrage
			x_1 = x_mitte;
		}
	}
	//Wenn unsere angestrebte Genauigkeit erreicht haben, geben wir den Wert als Nullstelle zurück
	return x_mitte;
}

/*
 * Diese Funktion stellt sowohl die Mathematische Funktion im geraden als auch ungeraden Fall dar
 * Parameter: Funktionsvariable x, das berechnete xi, 0 für gerade oder 1 für ungerade Symmetrie
 * Return: Funktionswert als double
 */
double f(double x, double xi, short symmetrie){
	if(symmetrie == 0){ //Ich soll die Funktion für die gerade Symmetrie verwenden
		return tan(x)-(sqrt(pow(xi,2)-pow(x,2)))/(x);
	}else{//Ich soll die Funktion für die ungerade Symmetrie verwenden
		return (-1)/(tan(x))-(sqrt(pow(xi,2)-pow(x,2)))/(x);
	}
}

/*
 * Diese Funktion sortiert unsere Energieeigenwerte mit der Bubbelsort-Methode
 * Parameter: Zeiger auf das Array mit unseren gespeicherten Nullstellen, Zeiger auf das Array mit den gespeicherten symmetrien, laenge beider Arrays
 * Return: kein Return
 */
void bubblesort(double *nullstellen,char *symmetrie, int laenge){
	double temp;
	double tempSym;
	int n = laenge;
	int grenze;
	do{
		grenze = 1;
		for(int i=0; i<n-1; ++i){ //Einfacher Sortieralgorithmus
			if(nullstellen[i] > nullstellen[i+1]){
				temp = nullstellen[i+1];
				nullstellen[i+1] = nullstellen[i];
				nullstellen[i] = temp;
				
				tempSym = symmetrie[i+1];
				symmetrie[i+1] = symmetrie[i];
				symmetrie[i] = tempSym;
				grenze = i+1;
			}
		}
		n = grenze;
	}while(n > 1);
}

/*
 * Diese Funktion regelt unsere Ausgabe
 * Parameter: Potentialtopfbreite a in [m], Potentialtopftiefe in [J], Zeiger auf das Array mit unseren gespeicherten Nullstellen, Zeiger auf das Array mit den gespeicherten symmetrien
 * 				,Anzahl der Geraden lösungen, Anzahl der ungeraden Lösungen
 * Return: kein Return
 */
void ausgabe(double a, double v_0, double* nullstellen, char* symmetrie, int anzahlGerade, int anzahlUngerade){
	v_0 /= eV; //Es wird zurück in [eV] umgerechnet für die Ausgabe
	a/=pow(10,-12); //Es wird zurück in [pm] umgerechnet für die Ausgabe
	printf("\n\n+------------------------------+\n");
	printf("Der Potentialtopf hat:\nBreite: %dpm\nTiefe: %deV\n\n",(int)a, (int)v_0);
	printf("Es wurden %d Energieeigenwerte gefunden.\nDarunter %d gerade Lösungen und %d ungerade Lösungen.\n\n", anzahlGerade+anzahlUngerade, anzahlGerade, anzahlUngerade);
	printf("#\tEnergiewert\tSymmetrie\n");
	for(int i=0; i<anzahlGerade+anzahlUngerade; i++){
		printf("%d\t%feV\t%c\n", i+1, nullstellen[i], symmetrie[i]);
	}
	printf("+------------------------------+\n");
}
